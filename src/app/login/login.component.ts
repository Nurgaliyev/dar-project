import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../shared/auth.service';
import { AuthType } from './login.types';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  loginType: AuthType;

  constructor(
    private router: Router, 
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      login: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
    });
  }

  login() {
    if (!this.form.valid) {
      return;
    }
    const loginType = {
      username: this.form.get('login').value,
      password: this.form.get('password').value
    }
    this.authService.postUser(loginType)
      .subscribe(
        res => {
          localStorage.setItem('dar-lab-auth', res['token']);
          this.router.navigate(['/'])
        },
          err => {
            alert(err['error']['error']);
          }
      );
  }
}
