import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../shared/auth.service';
import { AuthType } from '../login/login.types';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  form: FormGroup;
  loginType: AuthType;

  constructor(
  	private router: Router, 
    private authService: AuthService
    ) { }

  ngOnInit() {
  	this.form = new FormGroup({
      login: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
    });
  }


  register(){
  	if(!this.form.valid){
  		return;
  	}
  	const loginType = {
      username: this.form.get('login').value,
      password: this.form.get('password').value
    }
    this.authService.registerUser(loginType)
    .subscribe(
    	res => {
    		this.router.navigate(['login']);
    	}
    	);
  }

  

}
