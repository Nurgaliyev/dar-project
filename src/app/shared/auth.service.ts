import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthType } from '../login/login.types';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

	host = 'http://localhost:3000'


	constructor(private http: HttpClient) {

	}

	postUser(data: AuthType): Observable<AuthType>{
		return this.http.post<AuthType>(`${this.host}/users/auth`, data);
	}
	registerUser(data: AuthType): Observable<AuthType>{
		return this.http.post<AuthType>(`${this.host}/users`, data);
	}
}